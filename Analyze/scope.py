#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 12 22:38:59 2018

@author: luddi
"""

import serial
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.style as style

fs = 1000    # Sampling frequency
data = np.array([0,0,0])

try:
    with serial.Serial('/dev/ttyACM1', 115200, timeout=0.5) as ser:

        while True:
            line = ser.readline().decode("utf-8")
            sample = np.array(line[:-2].split(','))
            sample = sample.astype(np.int)
            data = np.vstack([data, sample])
            
except KeyboardInterrupt:
    pass


t = np.linspace(0, len(data)/fs, num=len(data))

style.use('seaborn-darkgrid')
#plt.plot(t, data[:,0], label='X')
#plt.plot(t, data[:,1], label='Y')
plt.plot(t, data[:,2], label='Z')
plt.legend()
plt.show()
