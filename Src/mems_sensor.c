/**
 ******************************************************************************
 * @file    mems_sensor.c
 * @author  L.Horsthemke
 * @version V1.0.0
 * @date    24.02.2017
 * @brief   Interface to IIS328DQ MEMS acceleration sensor
 *
 *
 ******************************************************************************
 *
 * COPYRIGHT(c) 2017 RT-Systemtechnik
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "mems_sensor.h"
#include "stm32f4xx_hal.h"
#include "i2c.h"

extern I2C_HandleTypeDef hi2c1;

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/


/* Private function prototypes -----------------------------------------------*/
/* Exported functions --------------------------------------------------------*/

/**
 * Initializes mems sensor
 * @return
 */
EnStatus_t mems_initSensor(void)
{
  EnStatus_t error = STATUS_OK;
  uint8_t confBuf = 0x00;

  /** configure ctrl register 1 */
  confBuf = IIS328DQ_NORMAL_MODE|IIS328DQ_ODR1000|IIS328DQ_XEN|IIS328DQ_YEN|IIS328DQ_ZEN;
  if (HAL_I2C_Mem_Write(&hi2c1, IIS328DQ_I2C_ADDRESS_HIGH, IIS328DQ_CTRL_REG1, I2C_MEMADD_SIZE_8BIT, &confBuf, 1, I2C_TIMEOUT ) != HAL_OK)
  {
    error = STATUS_ERROR;
  }

  /** configure ctrl register 2 */
  confBuf = IIS328DQ_HPM_NORMAL|IIS328DQ_HPC8|IIS328DQ_FDS;
  if (HAL_I2C_Mem_Write(&hi2c1, IIS328DQ_I2C_ADDRESS_HIGH, IIS328DQ_CTRL_REG2, I2C_MEMADD_SIZE_8BIT, &confBuf, 1, I2C_TIMEOUT ) != HAL_OK)
  {
    error = STATUS_ERROR;
  }

  /** configure ctrl register 4 */
  confBuf = IIS328DQ_BDU|IIS328DQ_FS8G;
  if (HAL_I2C_Mem_Write(&hi2c1, IIS328DQ_I2C_ADDRESS_HIGH, IIS328DQ_CTRL_REG4, I2C_MEMADD_SIZE_8BIT, &confBuf, 1, I2C_TIMEOUT ) != HAL_OK)
  {
    error = STATUS_ERROR;
  }

  return error;
}

/**
 * De-initializes mems sensor
 * @return
 */
EnStatus_t mems_deinitSensor(void)
{
  EnStatus_t error = STATUS_OK;
  uint8_t confBuf = 0x00;

  /** configure ctrl register 1 to power down sensor */
  confBuf = IIS328DQ_POWER_DOWN;
  if (HAL_I2C_Mem_Write(&hi2c1, IIS328DQ_I2C_ADDRESS_HIGH, IIS328DQ_CTRL_REG1, I2C_MEMADD_SIZE_8BIT, &confBuf, 1, I2C_TIMEOUT ) != HAL_OK)
  {
    error = STATUS_ERROR;
  }

  return error;
}

/**
 * Starts readout of MEMS sensor via DMA, reception of data is done in DMA RxCplt callback
 * @param pData buffer to store received data in, has to be at least 6 bytes long
 * @return
 */
EnStatus_t mems_startRead(u8 *pData)
{
  EnStatus_t error = STATUS_OK;

  /** burst read registers, starting at out x low register*/
  if (HAL_I2C_Mem_Read_DMA( &hi2c1, IIS328DQ_I2C_ADDRESS_HIGH, IIS328DQ_AUTO_INC|IIS328DQ_OUT_X_L, I2C_MEMADD_SIZE_8BIT, pData, 6 ) != HAL_OK)
  {
    error = STATUS_ERROR;
  }

  return error;
}

/************************ (C) COPYRIGHT RT-Systemtechnik ********END OF FILE****/
