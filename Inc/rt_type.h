/**
  ******************************************************************************
  * @file    rt_type.h
  * @author  l.horsthemke
  * @version V1.0.0
  * @date    22.02.2017
  * @brief   Type conventions used at RT
  *
  ******************************************************************************
  *
  * COPYRIGHT(c) 2017 RT-Systemtechnik
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef RT_TYPE_H_
#define RT_TYPE_H_

#include <stdint.h>
#include <stdbool.h>

/**
 * typedefs from stdint to RT convention
 */
typedef uint64_t            u64;
typedef uint32_t            u32;
typedef uint16_t            u16;
typedef uint8_t             u8;

typedef int64_t             s64;
typedef int32_t             s32;
typedef int16_t             s16;
typedef int8_t              s8;

typedef volatile uint32_t   vu32;
typedef volatile uint16_t   vu16;
typedef volatile uint8_t    vu8;

typedef volatile int32_t    vs32;
typedef volatile int16_t    vs16;
typedef volatile int8_t     vs8;


/**
  * @brief  Status structures definition
  */
typedef enum
{
  STATUS_OK       = 0x00U,
  STATUS_ERROR    = 0x01U,
  STATUS_BUSY     = 0x02U,
  STATUS_TIMEOUT  = 0x03U
} EnStatus_t;


#endif /* RT_TYPE_H_ */

/************************ (C) COPYRIGHT Systemtechnik **********END OF FILE****/
