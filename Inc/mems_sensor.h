/**
 ******************************************************************************
 * @file    mems_sensor.h
 * @author  L.Horsthemke
 * @version V1.0.0
 * @date    24.02.2017
 * @brief   Interface to IIS328DQ MEMS acceleration sensor
 *
 ******************************************************************************
 *
 * COPYRIGHT(c) 2017 RT-Systemtechnik
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef MEMS_SENSOR_H_
#define MEMS_SENSOR_H_


/* Includes ------------------------------------------------------------------*/
#include "main.h"   /**< include main.h for global defines */

/* Private macros ------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/

/**
 * @brief  Sensor axes data structure definition
 */
typedef struct
{
  s16 AXIS_X;
  s16 AXIS_Y;
  s16 AXIS_Z;
} StSensorAxes_t;

/* Exported constants --------------------------------------------------------*/

#define I2C_TIMEOUT   500

/************** I2C Address *****************/
#define IIS328DQ_I2C_ADDRESS_LOW   (0x18<<0x1U)  /**< SAD[0] = 0 */
#define IIS328DQ_I2C_ADDRESS_HIGH  (0x19<<0x1U)  /**< SAD[0] = 1 */

/************** Who am I  *******************/
#define IIS328DQ_WHO_AM_I         0x32

/************** Device Register  *******************/
#define IIS328DQ_CTRL_REG1    0x20
#define IIS328DQ_CTRL_REG2    0x21
#define IIS328DQ_CTRL_REG3    0x22
#define IIS328DQ_CTRL_REG4    0x23
#define IIS328DQ_CTRL_REG5    0x24
#define IIS328DQ_HP_FILTER_RESET    0x25
#define IIS328DQ_REFERENCE    0x26
#define IIS328DQ_STATUS_REG    0x27
#define IIS328DQ_OUT_X_L    0x28
#define IIS328DQ_OUT_X_H    0x29
#define IIS328DQ_OUT_Y_L    0x2A
#define IIS328DQ_OUT_Y_H    0x2B
#define IIS328DQ_OUT_Z_L    0x2C
#define IIS328DQ_OUT_Z_H    0x2D
#define IIS328DQ_INT1_CFG    0x30
#define IIS328DQ_INT1_SRC    0x31
#define IIS328DQ_INT1_THS    0x32
#define IIS328DQ_INT1_DURATION    0x33
#define IIS328DQ_INT2_CFG    0x34
#define IIS328DQ_INT2_SRC    0x35
#define IIS328DQ_INT2_THS    0x36
#define IIS328DQ_INT2_DURATION    0x37

/************** Device Register BITS *******************/
#define IIS328DQ_POWER_DOWN       0x0
#define IIS328DQ_NORMAL_MODE      (1 << 5)
#define IIS328DQ_LP_2             (0x4U << 5)
#define IIS328DQ_LP_5             (0x5U << 5)
#define IIS328DQ_LP_10            (0x6U << 5)
#define IIS328DQ_ODR50            0x0
#define IIS328DQ_ODR100           (1 << 3)
#define IIS328DQ_ODR400           (1 << 4)
#define IIS328DQ_ODR1000          (0x3U << 3)
#define IIS328DQ_ZEN              (1 << 2)
#define IIS328DQ_YEN              (1 << 1)
#define IIS328DQ_XEN              (1 << 0)

#define IIS328DQ_HPM_NORMAL       0x0
#define IIS328DQ_HPM_REF          (1 << 5)
#define IIS328DQ_FDS              (1 << 4)
#define IIS328DQ_HPC8             0x0
#define IIS328DQ_HPC16            0x1
#define IIS328DQ_HPC32            0x2
#define IIS328DQ_HPC64            0x3

#define IIS328DQ_BDU              (1 << 7)
#define IIS328DQ_FS2G             0x0
#define IIS328DQ_FS4G             (1 << 4)
#define IIS328DQ_FS8G             (0x3U << 4)

/**
 *  If the MSb of the SUB field is �1�, the SUB (register address)
 *  is automatically increased to allow multiple data read/write.
 */
#define IIS328DQ_AUTO_INC         (1 << 7)



/* Exported macros -----------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/

EnStatus_t mems_startRead(u8 *pData);

/* Initialization and de-initialization functions  ****************************/
EnStatus_t mems_initSensor(void);
EnStatus_t mems_deinitSensor(void);

/* Peripheral Control functions  **********************************************/

#endif /* MEMS_SENSOR_H_ */

/************************ (C) COPYRIGHT Systemtechnik **********END OF FILE****/
